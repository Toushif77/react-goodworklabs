import React, {Component} from 'react';
import './App.css';

let globalStore = [];

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tableState: [],
            permanentState: []
        }
    }

    componentDidMount() {
        fetch("https://api.myjson.com/bins/109m7i", {
          method: 'GET',
        }).then((response) => {
          return response.json();
        }).then((response) => {
          let storeCategory = [], storeArray = [];

          /*Only 1 iteration will happen for best performance*/
          response.forEach((obj, ind) => {
            if(storeCategory.includes(obj.category.replace(/[^a-zA-Z ]/g, ""))) {
               storeArray.push(<tr key={ind}>
                                  <td className={!obj.stocked ? "isStocked" : ''}>
                                      {obj.name}
                                  </td>
                                  <td>
                                      {obj.price}
                                  </td>
                              </tr>)
            } else {
              storeArray.push(<tr key={"key"+ind}>
                                <th id={ind}>
                                    {obj.category}
                                </th>
                            </tr>)
              storeArray.push(<tr key={ind}>
                                <td className={!obj.stocked ? "isStocked" : ''}>
                                    {obj.name}
                                </td>
                                <td>
                                    {obj.price}
                                </td>
                            </tr>)
            }

            if(!storeCategory.includes(obj.category.replace(/[^a-zA-Z ]/g, ""))) {
              storeCategory.push((obj.category).replace(/[^a-zA-Z ]/g, ""));
            }
          })

          this.setState({
            tableState: storeArray,
            permanentState: storeArray
          })
          globalStore = [...storeArray];
        }).catch((error) => {
            console.log(error);
        });
    }

    searchChange = (event) => {
      let keepArray = [];

      if(event.target.value !== '') {
        keepArray = globalStore.filter((val, i) => {
            return val.props.children.length !== undefined ? val.props.children[0].props.children.toLowerCase().includes(event.target.value.toLowerCase()) : false;
        })
      } else {
        keepArray = Object.assign([], globalStore);
      }
      this.selectStocked(this.addCategories(keepArray));

    }

    selectStocked = (event) => {
      const { tableState, permanentState } = this.state;
      let checkArray = [], storePrev = [];
      checkArray = (typeof event.length === 'number') ? event : tableState;
      storePrev = (typeof event.length === 'number') ? Object.assign([], event) : Object.assign([], tableState);

      if(document.querySelector('.messageCheckbox').checked) {
        checkArray.forEach((val, i) => {
          if(val.props.children.length !== undefined) {
            val.props.children.forEach(v => {
              if(v.props.className === "isStocked") {
                checkArray.splice(i, 1);
              }
            })
          }
        })
      }

      this.setState({
        tableState: document.querySelector('.messageCheckbox').checked ? checkArray : (typeof event.length === 'number' ? storePrev : permanentState),
        permanentState: storePrev
      })
    }

    addCategories = (rows) => {
      let category = [];
      globalStore.forEach((val, ind) => {
        typeof val.props.children.length !== 'number' ? (category.length === 1 ? (category[0] = ind) : category.push(ind))
        :
        rows.forEach((v, i) => {
          if(ind > i) {
            if(v === val) {
              if(category.length === 1) {
                console.log('row1', rows);
                rows.splice(i, 0, globalStore[category[0]]);
                category.pop();
              }
            }
          }
        })
      })
      return rows;
    }

  render() {

    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-sm-12 outer-wrapper">
              <div className="stock-wrapper row">
                <div className="search col-md-12 col-sm-12">
                  <form className="form-horizontal" action="">
                    <div className="form-group">
                      <input type="text" className="form-control searchTerm" placeholder="Search..." onChange={event => this.searchChange(event)}/>
                    </div>
                    <div className="form-group checkTerm">
                      <input type="checkbox" className="form-control messageCheckbox" onChange={e => this.selectStocked(e)}/><span className="check-text">Only show products in stock</span>
                    </div>
                  </form>
               </div>
               <div className="col-md-12 col-sm-12 table-design">
                    <div className="section-style">
                      {
                        this.state.tableState.length !== 0
                        ?
                        <table>
                          <thead>
                              <tr>
                                  <th>
                                      Name
                                  </th>
                                  <th>
                                      Price
                                  </th>
                              </tr>
                          </thead>
                          <tbody>
                              {this.state.tableState}
                          </tbody>
                        </table>
                        :
                        <p className="check-text">No records found.</p>
                      }
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;